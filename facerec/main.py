import cv2
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model

img_rows, img_cols = 64, 64

train_data_dir = 'C:/HomeWork/AI/artificial-intelligence/facerec/nikita/humandetect-master-train/train'
batch_size = 50

train_datagen = ImageDataGenerator(
    rescale=1./255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size,
    class_mode='binary')

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(img_rows, img_cols, 3)))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(filters=64, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(filters=64, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(64))
model.add(Activation("relu"))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation("sigmoid"))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])


model.fit_generator(
    train_generator,
    steps_per_epoch=train_generator.n // batch_size,
    epochs=10)


model.save('model.h5')
model.summary()

model = load_model('model.h5')


cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cap.set(cv2.CAP_PROP_EXPOSURE, -3)
while True:
    
    ret, frame = cap.read()

    
    resized_frame = cv2.resize(frame, (img_rows, img_cols))

    
    img_array = np.array(resized_frame)

 
    img_array = np.expand_dims(img_array, axis=0)

    
    img_array = img_array / 255.

    
    prediction = model.predict(img_array)
    

    
    if prediction[0][0] > 0.5:
        cv2.putText(frame, f'Human {prediction}', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    else:
        cv2.putText(frame, f'No {prediction}', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    
    cv2.imshow('frame', frame)

    
    if cv2.waitKey(1) & 0xFF == ord('d'):
        break


cap.release()
cv2.destroyAllWindows()
