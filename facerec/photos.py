import cv2
import os
import time


# Создаем папку для сохранения фотографий
if not os.path.exists('datasets/with_human'):
    os.makedirs('datasets/with_human')

# Запускаем камеру
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cap.set(cv2.CAP_PROP_EXPOSURE, -5)

# Счетчик фотографий
count = 0

while count < 2700:
    # Считываем кадр с камеры
    # time.sleep(5)
    ret, frame = cap.read()

    
    cv2.imshow('frame', frame)

    # Обработка нажатия клавиши
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

 # Сохраняем фотографию
    if count % 10 == 0:
        filename = f'C:/HomeWork/AI/facerec/datasets/without_human/{count}.jpg'
        cv2.imwrite(filename, frame)
        print(f'Saved {filename}')
        # time.sleep(1)

    
    # Увеличиваем счетчик
    count += 1

# Освобождаем ресурсы
cap.release()
cv2.destroyAllWindows()
