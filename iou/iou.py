# def iou(bbox1: list, bbox2: list) -> float:
#     x = max(bbox1[0], bbox2[0])
#     y = max(bbox1[1], bbox2[1])
#     x2 = min(bbox1[2], bbox2[2])
#     y2 = min(bbox1[3], bbox2[3])
    
#     interA = max(0, x2-x +1) * min(0, y2-y+1)
#     boxA = (bbox1[2]-bbox1[0] +1) * (bbox2[3]-bbox2[1]+1)
#     bboxA = (bbox2[2]-bbox2[0]+ 1) * (bbox2[3] - bbox2[1] +1)
#     iou = interA  / float(boxA + bboxA - interA)
#     return iou

def iou(bbox1:list, bbox2:list) -> float:
    weight = min(bbox1[2]+bbox1[3], bbox2[2]+bbox2[3]) - max(bbox1[2], bbox2[2])
    height = min(bbox1[0]+bbox1[1], bbox2[0]+bbox2[1]) - max(bbox1[0], bbox2[0])
    
    if (weight<0) or (height<0):
        inter_area = 0.0
    else:
        inter_area = weight*height
    union_area = bbox1[1]*bbox1[3]
    try:
        iou = float(inter_area/union_area)
    except Exception:
        iou = 0.0
    
    return iou

    



bbox1 = [0, 1, 0, 10]
bbox2 = [0, 10, 1, 10]
bbox3 = [20, 30, 20, 30]
bbox4 = [5, 15, 5, 15]
assert iou(bbox1, bbox1) == 1.0
assert iou(bbox1, bbox2) == 0.9
assert iou(bbox1, bbox3) == 0.0
assert round(iou(bbox1, bbox4), 2) == 0.14
