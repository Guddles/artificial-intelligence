import cv2
import numpy as np
import matplotlib.pyplot as plt

cam = cv2.VideoCapture(0)
cv2.namedWindow("Camera", cv2.WINDOW_FULLSCREEN)

glasses = cv2.imread("dealwithit.png", -1)
eye = cv2.CascadeClassifier("haarcascade_eye.xml")
face = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")




while cam.isOpened():
    ret, frame = cam.read()
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2BGRA)
    
    for (x,y,w,h) in faces:
        _gray = gray[y:y+h, x:x+h]
        _color = frame[y:y+h, x:x+h]
        cv2.rectangle(frame, (x,y), (x+w, y+h), (255,255,255),3)
        
        eyes = eye.detectMultiScale(_gray, scaleFactor=2.5, minNeighbors=5)
        for (x1,y1,w1,h1) in eyes:
            # cv2.rectangle(_color, (x1,y1), (x1+w1, y1+h1), (255,255,0),3)
            roi_eyes = _gray[y1: y1+h1, x1:x1+w1]
            glasses = cv2.resize(glasses, (w1,h1), interpolation = cv2.INTER_AREA)
            
            gw, gh, gc = glasses.shape
            for i in range(0, gw):
                for j in range(0, gh):
                    if glasses[i,j][3]!=0:
                        _color[y1+i, x1+j] = glasses[i,j]
            
    
    
    
    
    frame = cv2.cvtColor(frame, cv2.COLOR_BGRA2BGR)
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key>0:
        if chr(key) == 'd':
            break
cam.release()
cv2.destroyAllWindows()

