import cv2
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model

draw = False
model = load_model('mnist.h5')
def draw_callback(event, x, y, flags, param):
    global draw
    if event == cv2.EVENT_MOUSEMOVE:
        if draw:
            cv2.circle(img, (x,y), 5, 200, 5)
    if event == cv2.EVENT_LBUTTONDOWN:
        draw = True
    elif event == cv2.EVENT_LBUTTONUP:
        draw = False


img = np.zeros((512, 512), dtype="uint8")

cv2.namedWindow("mnist")
cv2.setMouseCallback("mnist", draw_callback)




while True:
    cv2.imshow("mnist", img)
    
    key = cv2.waitKey(1)
    if key == ord("d"):
        break
    if key == ord("s"):
        print("Recognize")
        
        img = img / 255.
        img = cv2.resize(img, (28, 28))
        
        
               
        ar = img.reshape((1, img.shape[0], img.shape[1], 1))
        print(ar.shape)
        predictions = model.predict(ar)
        # np.round(predictions[0], 2)
        res = np.argmax(predictions)
        print(res)
        
        
    if key == ord("c"):
        print("Clear")
        img[:] = 0

cv2.destroyAllWindows()