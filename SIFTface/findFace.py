import cv2
import numpy as np

cam = cv2.VideoCapture(0)
cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)


ret, frame = cam.read()
frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
while cam.isOpened():
    ret, frame2 = cam.read()
    frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    sift = cv2.SIFT_create()
    
    key_points1, description1 = sift.detectAndCompute(frame, None)
    key_points2, description2 = sift.detectAndCompute(frame2, None)
    
    
    
    matcher = cv2.BFMatcher()
    matches =  matcher.knnMatch(description1, description2, k=2)
    
    best = []
    for m1, m2 in matches:
        if m1.distance < 0.75* m2.distance:
            best.append([m1])

    print(f"All matches {len(matches)}, best matches {len(best)}")

    if len(best)>0:
        src_pts = np.float32([key_points1[m[0].queryIdx].pt for m in best]).reshape(-1,1,2)
        dst_pts = np.float32([key_points2[m[0].trainIdx].pt for m in best]).reshape(-1,1,2)
        M, hmask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        w,h = frame.shape
        pts = np.float32([[0,0], [0, h-1], [w-1, h-1], [w-1, 0]]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts, M)
        result = cv2.polylines(frame2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
    else:
        print("Not enough matches")
        mask = None
        
    matches_image = cv2.drawMatchesKnn(frame, key_points1, frame2, key_points2, best, None)
    
    # cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key>0:
        if chr(key) == 'd':
            break
        if chr(key) == 's':
            x, y, w, h = cv2.selectROI("select", frame2)
            frame = frame2[int(y):int(y+h), int(x):int(x+w)]
            cv2.destroyWindow("select")
    cv2.imshow("Camera", result)
            
cam.release()
cv2.destroyAllWindows()